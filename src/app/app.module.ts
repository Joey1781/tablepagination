import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import { ApiService } from './services/api.service';
import { FormsModule } from '@angular/forms';
import { EmployerService } from './services/employer.service';

@NgModule({
  declarations: [
    AppComponent,
    PageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ApiService, EmployerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
