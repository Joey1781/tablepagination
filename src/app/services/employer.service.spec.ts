import { TestBed, inject } from '@angular/core/testing';

import { EmployerService } from './employer.service';
import { HttpClientModule } from '@angular/common/http';

describe('EmployerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [EmployerService]
    });
  });

  it('should be created', inject([EmployerService], (service: EmployerService) => {
    expect(service).toBeTruthy();
  }));
});
