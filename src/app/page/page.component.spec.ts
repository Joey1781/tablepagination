import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PageComponent } from './page.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { EmployerService } from '../services/employer.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('PageComponent', () => {
  let component: PageComponent;
  let fixture: ComponentFixture<PageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule,
        HttpClientTestingModule
      ],
      declarations: [ PageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('it retrieve all the info', async(inject( [EmployerService], ( employerService ) => {
    employerService.getAllEmployers().subscribe(result => expect(result.length).toBeGreaterThan(0)); 
  })));

  it(`should issue a Post request`,
  // 1. declare as async test since the HttpClient works with Observables
  async(
    // 2. inject HttpClient and HttpTestingController into the test
    inject([EmployerService, HttpTestingController], (employerService: EmployerService, backend: HttpTestingController) => {
      // 3. send a simple request
      employerService.submitOneEmployer({}).subscribe();

      // 4. HttpTestingController supersedes `MockBackend` from the "old" Http package
      // here two, it's significantly less boilerplate code needed to verify an expected request
      backend.expectOne({
        url: 'api/submit',
        method: 'Post'
      });
    })
  )
);

});
